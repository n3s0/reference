"""
file: core.py
description: Contains core functionality for reference-cli.
"""

def banner():
    banner = r"""
    ______      __                             
    | ___ \    / _|                            
    | |_/ /___| |_ ___ _ __ ___ _ __   ___ ___ 
    |    // _ \  _/ _ \ '__/ _ \ '_ \ / __/ _ \
    | |\ \  __/ ||  __/ | |  __/ | | | (_|  __/
    \_| \_\___|_| \___|_|  \___|_| |_|\___\___|

    """
    return banner


def ref_modules():
    """ Lists the modules available in the script."""
    modules = ["natopa     : NATO Phonetic Alphabet",
               "int-morse  : International Morse Code"]

    print("Modules Listing\n")

    print("Below is a moule listing for the script:\n")

    for module in modules:
        print("* {}".format(module))

    print("")
