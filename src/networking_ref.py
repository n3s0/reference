"""
file: networking_ref.py
description: Provides references related to computer networking.
"""

def rfc_1918():
    """ Provides information from RFC 1918. For clarification, this is documentation
    for the different classes of IPv4 addresses. This is also intended to provide 
    important data from the RFC itself. """
