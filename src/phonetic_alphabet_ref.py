#!/usr/bin/python

"""
Title: NATO Phonetic Alphabet
Author: Timothy (n3s0)
File: src/modules/nato_phoetic_alphabet.py
Description: Provides a table for the NATO Phonetic Alphabet.
Credit:
    - To http://lookuptables.com for being a wonderful reference so I could jot these down and create my own tool for it.
"""

from prettytable import PrettyTable

def nato_pa():
    """ Print a table of the NATO Phonetic Alphabet."""
    nato_pa_list = [["A", "Alpha", "Al-fah"],
                    ["B", "Bravo", "Brah-voh"],
                    ["C", "Charlie", "Char-lee"],
                    ["D", "Delta", "Dell-tah"],
                    ["E", "Echo", "Eck-oh"],
                    ["F", "Foxtrot", "Foks-trot"],
                    ["G", "Golf", "Golf"],
                    ["H", "Hotel", "Hoh-tel"],
                    ["I", "India", "In-dee-ah"],
                    ["J", "Juliet", "Jew-lee-et"],
                    ["K", "Kilo", "Key-loh"],
                    ["L", "Lima", "Lee-mah"],
                    ["M", "Mike", "Mike"],
                    ["N", "November", "No-vem-ber"],
                    ["O", "Oscar", "Oss-car"],
                    ["P", "Papa", "Pah-pah"],
                    ["Q", "Quebec", "Keh-beck"],
                    ["R", "Romeo", "Row-me-oh"],
                    ["S", "Sierra", "See-air-rah"],
                    ["T", "Tango", "Tang-go"],
                    ["U", "Uniform", "Yoo-nih-form"],
                    ["V", "Victor", "Vik-tore"],
                    ["W", "Whiskey", "Wiss-key"],
                    ["X", "Xray", "Ecks-ray"],
                    ["Y", "Yankee", "Yang-key"],
                    ["Z", "Zulu", "Zoo-loo"],
                    ["0", "Zero", "Zee-ro"],
                    ["1", "One", "Wun"],
                    ["2", "Two", "Too"],
                    ["3", "Three", "Tree"],
                    ["4", "Four", "Fow-er"],
                    ["5", "Five", "Fife"],
                    ["6", "Six", "six"],
                    ["7", "Seven", "Sev-en"],
                    ["8", "Eight", "Ait"],
                    ["9", "Nine", "Nin-er"],
                    ["100", "Hundred", "Hun-dred"],
                    ["1000", "Thousand", "Tou-sand"]]

    table = PrettyTable(["Letter", "Written Word", "How To Say It"])

    for rec in nato_pa_list:
        table.add_row(rec)

    print("\n++++++++++ Phonetic Alphabet ++++++++++++\n")
    print("* Credit for Table goes to http://www.lookuptables.com/\n")
    print("++++++++ NATO Phonetic Alphabet +++++++++")
    print(table)

